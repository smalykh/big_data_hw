#!/bin/bash

#99.168.127.53 -- [20/May/2010:07:34:13 +0100] "GET /media/img/m-inact.gif HTTP/1.1" 200 2571 "http://www.example.com/" "Mozilla/5.0 (iPhone; U; CPU iPhone OS 3_1_3 like Mac OS X; en-us) AppleWebKit/528.18 (KHTML, like Gecko) Version/4.0 Mobile/7E18 Safari/528.16"

#67.195.114.50 -- [20/May/2010:07:35:27 +0100] "GET /post/261556/ HTTP/1.0" 404 15 "-" "Mozilla/5.0 (compatible; Yahoo! Slurp/3.0; http://help.yahoo.com/help/us/ysearch/slurp)"


declare -A ip_base
IP_BASE_LEN=10
LOG_LEN=100

if [ $# -eq 1 ]; then
	LOG_LEN=$1
fi

rand_ipv4 () {
	echo $(($RANDOM%256)).$(($RANDOM%256)).$(($RANDOM%256)).$(($RANDOM%256))
}

rand_ipv4_code() {
	if [ $(($RANDOM %2)) -eq 1 ]; then
		echo 200
	else
		echo 404
	fi
}

rand_ipv4_from_base() {
	index=$(($RANDOM % $IP_BASE_LEN))
	echo ${ip_base[$index]}
}
rand_data_size() {
	echo $RANDOM
}


for((i=0;i<IP_BASE_LEN;i=i+1))
do
	ip_base[$i]=$(rand_ipv4)
done

for((i=0;i<LOG_LEN;i=i+1))
do
	echo "$(rand_ipv4_from_base) -- [$(date)] \"GET /random/addr HTTP/1.1\" $(rand_ipv4_code) $(rand_data_size) \" http://example.com/\" \"Mozilla/5.0 (compatible; Yahoo! Slurp/3.0; http://help.yahoo.com/help/us/ysearch/slurp)\""
done
