
import java.io.IOException;
import java.io.DataInput;
import java.io.DataOutput;
import java.util.*;
	
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
	

public class Traffic {
	public static void main(String[] args) throws Exception {
		Configuration conf = new Configuration();
		conf.set("mapred.textoutputformat.separator", ",");

		Job job = new Job(conf, "wordcount");
		job.setJarByClass(Traffic.class);

		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(com.onefoursix.traffic.TrafficStat.class);
	
		job.setMapperClass(com.onefoursix.traffic.TrafficMapper.class);
		job.setCombinerClass(com.onefoursix.traffic.TrafficReducer.class);
		job.setReducerClass(com.onefoursix.traffic.TrafficReducer.class);
	
		job.setInputFormatClass(TextInputFormat.class);
		job.setOutputFormatClass(TextOutputFormat.class);
	
		FileInputFormat.addInputPath(job, new Path(args[0]));
		FileOutputFormat.setOutputPath(job, new Path(args[1]));

		job.waitForCompletion(true);
 	}	
}
