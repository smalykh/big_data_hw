package com.onefoursix.traffic;
	
import java.io.IOException;
import java.io.DataInput;
import java.io.DataOutput;
import java.util.*;
	
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
	

/* To keep information about transmitted bytes and requests from IP */
public class TrafficStat implements Writable {
	public long Traffic;
	public int Requests;

	public TrafficStat() { }

	public TrafficStat(int t, int r) {
		Traffic = t;
		Requests = r;
	}

	public void write(DataOutput out) throws IOException {
		out.writeLong(Traffic);
		out.writeInt(Requests);
	}

	public void readFields(DataInput in) throws IOException {
		Traffic = in.readLong();
		Requests = in.readInt();
	}

	@Override
	public int hashCode() {
		return (int)Traffic + (int)Requests<<16;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrafficStat other = (TrafficStat) obj;
		if (Traffic != other.Traffic)
			return false;
		if (Requests != other.Requests)
			return false;
		return true;
	}

	public String toString() {
		return Double.toString((double)Traffic / Requests) + "," + Long.toString(Traffic);
	}
}


