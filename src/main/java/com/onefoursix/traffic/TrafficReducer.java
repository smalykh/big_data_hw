package com.onefoursix.traffic;
	
import java.io.IOException;
import java.io.DataInput;
import java.io.DataOutput;
import java.util.*;
	
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
	
public class TrafficReducer extends Reducer<Text, TrafficStat, Text, TrafficStat> {
	public void reduce(Text key, Iterable<TrafficStat> values, Context context) 
		throws IOException, InterruptedException {
		TrafficStat ts = new TrafficStat(0, 0);
		for (TrafficStat value : values) {
			ts.Traffic  += value.Traffic;
			ts.Requests += value.Requests;
		}
		context.write(key, ts);
	}
}
