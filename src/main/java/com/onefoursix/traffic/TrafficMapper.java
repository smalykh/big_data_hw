package com.onefoursix.traffic;

import java.io.IOException;
import java.io.DataInput;
import java.io.DataOutput;
import java.util.*;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

public class TrafficMapper extends Mapper<LongWritable, Text, Text, TrafficStat> {
	public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
		String s = value.toString();
		String ip = s.split(" ")[0];
		int bytes = Integer.parseInt(s.split("\"")[2].split(" ")[2]);

		context.write(new Text(ip), new TrafficStat(bytes, 1));
	}
} 

