package com.onefoursix.traffic;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.mapreduce.MapReduceDriver;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.junit.Before;

public abstract class TrafficTestBase {
	MapReduceDriver<LongWritable, Text, Text, TrafficStat, Text, TrafficStat> mapReduceDriver;
	MapDriver<LongWritable, Text, Text, TrafficStat> mapDriver;
	ReduceDriver<Text, TrafficStat, Text, TrafficStat> reduceDriver;

	@Before
	public void setUp() {
		TrafficMapper mapper = new TrafficMapper();
		TrafficReducer reducer = new TrafficReducer();
		mapDriver = new MapDriver<LongWritable, Text, Text, TrafficStat>();
		mapDriver.setMapper(mapper);
		reduceDriver = new ReduceDriver<Text, TrafficStat, Text, TrafficStat>();
		reduceDriver.setReducer(reducer);
		mapReduceDriver = new MapReduceDriver<LongWritable, Text, Text, TrafficStat, Text, TrafficStat>();
		mapReduceDriver.setMapper(mapper);
		mapReduceDriver.setReducer(reducer);
	}

}
