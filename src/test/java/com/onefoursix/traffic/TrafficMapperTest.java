package com.onefoursix.traffic;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.junit.Test;


public class TrafficMapperTest extends TrafficTestBase {
	static String LogRec = "67.195.114.50 -- [20/May/2010:07:35:27 +0100] \"GET /post/261556/ HTTP/1.0\" 404 15 \"-\" \"Mozilla/5.0 (compatible; Yahoo! Slurp/3.0; http://help.yahoo.com/help/us/ysearch/slurp)\"";
	
	@Test
	public void testMapper1() {
		mapDriver.withInput(new LongWritable(1), new Text(LogRec));
		mapDriver.withOutput(new Text("67.195.114.50"), new TrafficStat(15, 1));
		mapDriver.runTest();
	}
	
	/* if uncommented, this test will fail */
	/*
	@Test
	public void testMapperWithZebrasAndHorses() {
		mapDriver.withInput(new LongWritable(1), new Text("horse zebra horse"));
		mapDriver.withOutput(new Text("horse"), new IntWritable(1));
		mapDriver.withOutput(new Text("horse"), new IntWritable(1));
		mapDriver.withOutput(new Text("zebra"), new IntWritable(1));
		mapDriver.runTest();
	}
	*/
	


}
