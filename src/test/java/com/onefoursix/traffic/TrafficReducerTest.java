package com.onefoursix.traffic;

import java.util.ArrayList;
import java.util.List;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.junit.Test;

public class TrafficReducerTest extends TrafficTestBase {
	
	@Test
	public void testReducerWithCats() {
		List<TrafficStat> values = new ArrayList<TrafficStat>();
		values.add(new TrafficStat(1024,1));
		values.add(new TrafficStat(512, 1));
		Text ip = new Text("1.1.1.1");
		reduceDriver.withInput(ip, values);
		reduceDriver.withOutput(ip, new TrafficStat(1536, 2));
		reduceDriver.runTest();
	}
	
	/* if uncommented, this test will fail */
	/*
	@Test
	public void testReducerWithZebras() {
		List<IntWritable> values = new ArrayList<IntWritable>();
		values.add(new IntWritable(1));
		values.add(new IntWritable(1));
		reduceDriver.withInput(new Text("horse"), values);
		reduceDriver.withOutput(new Text("zebra"), new IntWritable(2));
		reduceDriver.runTest();
	}
	*/
	

}
