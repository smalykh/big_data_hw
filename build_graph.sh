#!/bin/bash

for((i=32;i<$((1024*128));i=i*2))
do
	cd scripts
	./dumpdeploy.sh $i
	cd ..

	echo Records count $i
	echo Records count $i >> log
	time ./run.sh >> log 2>&1
done
